define jasm

element jasm.reg

element r1? : jasm.reg + 0
element r2? : jasm.reg + 1
element r3? : jasm.reg + 2
element r4? : jasm.reg + 3
element r5? : jasm.reg + 4
element r6? : jasm.reg + 5
element r7? : jasm.reg + 6
element r8? : jasm.reg + 7

; special registers 
element sp? : jasm.reg + 8 ; stack pointer

define @dest 
define @src

macro jasm.parse_operand ns, op
	; is operand an addr
	match [addr], op
		ns.type = 'mem'
		ns.memtype = 'imm'
		ns.imm = addr ; store addr
		; match an expression
		match a-b, addr
			ns.type = 'expression'
			ns.operation = 'sub'
			ns.edest = a
			ns.esrc = b
		else match a+b, addr 
			ns.type = 'expression'
			ns.operation = 'add'
			ns.edest = a
			ns.esrc = b
		end match 
			
		if addr metadata 1 relativeto jasm.reg 
			ns.memtype = 'reg'
		end if
		
	; if operand is imm or register
	else
		ns.type = 'imm'
		ns.imm = op
		if ns.imm eq ns.imm element 1 
			if ns.imm metadata 1 relativeto jasm.reg
				ns.type = 'reg'
			end if 
		end if 
	end match
end macro

; mov 
macro mov? dest*, src*
	jasm.parse_operand @dest, dest
	jasm.parse_operand @src, src
	
	; reg, imm
	if @dest.type = 'reg' & @src.type = 'imm'
		dw 01h
		dw (dest metadata 1 - jasm.reg)	
		dw @src.imm
	; reg, reg
	else if @dest.type = 'reg' & @src.type = 'reg'
		dw 02h
		dw (dest metadata 1 - jasm.reg)
		dw (src metadata 1 - jasm.reg)
	; [mem], imm
	else if @dest.type = 'mem' & @src.type = 'imm'
		; [imm], imm 	
		if @dest.memtype = 'imm'
			dw 03h 
			dw @dest.imm
			dw @src.imm
		; [reg], imm
		else if @dest.memtype = 'reg'			
			dw 04h 
			dw @dest.imm metadata 1 - jasm.reg
			dw @src.imm
		end if 
	; [mem], reg
	else if @dest.type = 'mem' & @src.type = 'reg'
		; [imm], reg 	
		if @dest.memtype = 'imm'
			dw 05h 
			dw @dest.imm
			dw @src.imm metadata 1 - jasm.reg
		; [reg], reg
		else if @dest.memtype = 'reg'			
			dw 06h 
			dw @dest.imm metadata 1 - jasm.reg
			dw @src.imm metadata 1 - jasm.reg
		end if 
	; reg, [mem]
	else if @dest.type = 'reg' & @src.type = 'mem'
		; reg, [imm]
		if @src.memtype = 'imm'
			dw 07h
			dw @dest.imm metadata 1 - jasm.reg
			dw @src.imm
		; reg, [reg]
		else if @src.memtype = 'reg'
			dw 08h
			dw @dest.imm metadata 1 - jasm.reg
			dw @src.imm metadata 1 - jasm.reg
		end if
	; special instruction starts at 80h
	else if @dest.type = 'reg' & @src.type = 'expression'
		; make sure first parameter is a register
		if @src.edest metadata 1 relativeto jasm.reg 
			if @src.operation = 'add'
				; opcode, destination register, source register for expression, imm
				dw 80h
				dw @dest.imm metadata 1 - jasm.reg
				dw @src.edest metadata 1 - jasm.reg
				dw @src.esrc
			else if @src.operation = 'sub'
				; opcode, destination register, source register for expression, imm
				dw 81h 
				dw @dest.imm metadata 1 - jasm.reg
				dw @src.edest metadata 1 - jasm.reg
				dw @src.esrc
			end if 
		else 
			err "invalid expression, register must be the first argument"
		end if
	else 
		err 'invalid destination operand'
	end if 
end macro

; jmp
macro jmp? dest*
	jasm.parse_operand @dest, dest
	
	; imm 
	if @dest.type = 'imm'
		dw 09h
		dw @dest.imm
	; reg
	else if @dest.type = 'reg'	
		dw 0ah
		dw (dest metadata 1 - jasm.reg)
	else 
		err 'invalid destination'
	end if
end macro 

; je - if cmp is equal then jump
macro je? dest*
	jasm.parse_operand @dest, dest

	; imm 
	if @dest.type = 'imm'
		dw 0bh
		dw @dest.imm
	; reg
	else if @dest.type = 'reg'	
		dw 0ch
		dw (dest metadata 1 - jasm.reg)
	else 
		err 'invalid destination'
	end if
end macro 

; add 
macro add? dest*, src* 
	jasm.parse_operand @dest, dest
	jasm.parse_operand @src, src
	
	if @dest.type = 'reg'
		; reg, imm
		if @src.type = 'imm'
			dw 0dh, (dest metadata 1 - jasm.reg)
			dw @src.imm
		; reg, reg
		else if @src.type = 'reg'
			dw 0eh, (dest metadata 1 - jasm.reg)
			dw (src metadata 1 - jasm.reg)
		end if
	else 
		err 'destination must be a register'
	end if 
end macro 

; sub 
macro sub? dest*, src* 
	jasm.parse_operand @dest, dest
	jasm.parse_operand @src, src
	
	if @dest.type = 'reg'
		; reg, imm
		if @src.type = 'imm'
			dw 0fh, (dest metadata 1 - jasm.reg)
			dw @src.imm
		; reg, reg
		else if @src.type = 'reg'
			dw 10h, (dest metadata 1 - jasm.reg)
			dw (src metadata 1 - jasm.reg)
		end if
	else 
		err 'destination must be a register'
	end if 
end macro 

; mul 
macro mul? dest*, src* 
	jasm.parse_operand @dest, dest
	jasm.parse_operand @src, src
	
	if @dest.type = 'reg'
		; reg, imm
		if @src.type = 'imm'
			dw 11h, (dest metadata 1 - jasm.reg)
			dw @src.imm
		; reg, reg	
		else if @src.type = 'reg'
			dw 12h, (dest metadata 1 - jasm.reg)
			dw (src metadata 1 - jasm.reg)
		end if
	else 
		err 'destination must be a register'
	end if 
end macro 

; div 
macro div? dest*, src* 
	jasm.parse_operand @dest, dest
	jasm.parse_operand @src, src
	
	if @dest.type = 'reg'
		; reg, imm
		if @src.type = 'imm'
			dw 13h, (dest metadata 1 - jasm.reg)
			dw @src.imm
		; reg, reg
		else if @src.type = 'reg'
			dw 14h, (dest metadata 1 - jasm.reg)
			dw (src metadata 1 - jasm.reg)
		end if
	else 
		err 'destination must be a register'
	end if 
end macro 

; cmp 
macro cmp? dest*, src* 
	jasm.parse_operand @dest, dest
	jasm.parse_operand @src, src
	
	if @dest.type = 'reg'
		; reg, imm
		if @src.type = 'imm'
			dw 15h
			dw (dest metadata 1 - jasm.reg)
			dw @src.imm
		; reg, reg
		else if @src.type = 'reg'
			dw 16h
			dw (dest metadata 1 - jasm.reg)
			dw (src metadata 1 - jasm.reg)
		end if
	else 
		err 'destination must be a register'
	end if 
end macro 

; push 
macro push? src* 
	jasm.parse_operand @src, src
	
	if @src.type = 'imm'
		dw 17h
		dw @src.imm
	else if @src.type = 'reg'
		dw 18h
		dw (src metadata 1 - jasm.reg)
	else 
		err 'invalid operand'
	end if
end macro 

; pop
macro pop? dest* 
	jasm.parse_operand @dest, dest
	
	if @dest.type = 'reg'
		dw 19h
		dw (dest metadata 1 - jasm.reg)
	else 
		err 'operand must be a register'
	end if
end macro 

; call 
macro call? dest*
	jasm.parse_operand @dest, dest
	
	; imm 
	if @dest.type = 'imm'
		dw 1ah
		dw @dest.imm
	; reg
	else if @dest.type = 'reg'	
		dw 1bh, (dest metadata 1 - jasm.reg)
	else 
		err 'invalid destination'
	end if
end macro 

; ret 
macro ret?
	dw 1ch
end macro

; -- DRAW OPCODES --
macro drawPixel? x*, y*, color*
	local @x, @y, @color
	jasm.parse_operand @x, x 
	jasm.parse_operand @y, y 
	jasm.parse_operand @color, color 

	if @x.type = 'reg' & @y.type = 'reg' & @color.type = 'reg' 
		dw 1dh
		dw (x metadata 1 - jasm.reg) 
		dw (y metadata 1 - jasm.reg) 
		dw (color metadata 1 - jasm.reg) 
	else 
		err 'arguments must be registers'
	end if  
end macro;

; and 
macro and? dest*, src* 
	jasm.parse_operand @dest, dest
	jasm.parse_operand @src, src
	
	if @dest.type = 'reg'
		; reg, imm
		if @src.type = 'imm'
			dw 1eh, (dest metadata 1 - jasm.reg)
			dw @src.imm
		; reg, reg
		else if @src.type = 'reg'
			dw 1fh, (dest metadata 1 - jasm.reg)
			dw (src metadata 1 - jasm.reg)
		end if
	else 
		err 'destination must be a register'
	end if 
end macro 

; or 
macro or? dest*, src* 
	jasm.parse_operand @dest, dest
	jasm.parse_operand @src, src
	
	if @dest.type = 'reg'
		; reg, imm
		if @src.type = 'imm'
			dw 20h, (dest metadata 1 - jasm.reg)
			dw @src.imm
		; reg, reg
		else if @src.type = 'reg'
			dw 21h, (dest metadata 1 - jasm.reg)
			dw (src metadata 1 - jasm.reg)
		end if
	else 
		err 'destination must be a register'
	end if 
end macro 

; nand 
macro nand? dest*, src* 
	jasm.parse_operand @dest, dest
	jasm.parse_operand @src, src
	
	if @dest.type = 'reg'
		; reg, imm
		if @src.type = 'imm'
			dw 22h, (dest metadata 1 - jasm.reg)
			dw @src.imm
		; reg, reg
		else if @src.type = 'reg'
			dw 23h, (dest metadata 1 - jasm.reg)
			dw (src metadata 1 - jasm.reg)
		end if
	else 
		err 'destination must be a register'
	end if 
end macro 

; nor 
macro nor? dest*, src* 
	jasm.parse_operand @dest, dest
	jasm.parse_operand @src, src
	
	if @dest.type = 'reg'
		; reg, imm
		if @src.type = 'imm'
			dw 24h, (dest metadata 1 - jasm.reg)
			dw @src.imm
		; reg, reg
		else if @src.type = 'reg'
			dw 25h, (dest metadata 1 - jasm.reg)
			dw (src metadata 1 - jasm.reg)
		end if
	else 
		err 'destination must be a register'
	end if 
end macro

; xor 
macro xor? dest*, src* 
	jasm.parse_operand @dest, dest
	jasm.parse_operand @src, src
	
	if @dest.type = 'reg'
		; reg, imm
		if @src.type = 'imm'
			dw 26h, (dest metadata 1 - jasm.reg)
			dw @src.imm
		; reg, reg
		else if @src.type = 'reg'
			dw 27h, (dest metadata 1 - jasm.reg)
			dw (src metadata 1 - jasm.reg)
		end if
	else 
		err 'destination must be a register'
	end if 
end macro 

; xnor 
macro xnor? dest*, src* 
	jasm.parse_operand @dest, dest
	jasm.parse_operand @src, src
	
	if @dest.type = 'reg'
		; reg, imm
		if @src.type = 'imm'
			dw 28h, (dest metadata 1 - jasm.reg)
			dw @src.imm
		; reg, reg
		else if @src.type = 'reg'
			dw 29h, (dest metadata 1 - jasm.reg)
			dw (src metadata 1 - jasm.reg)
		end if
	else 
		err 'destination must be a register'
	end if 
end macro 

; not 
macro not? dest*
	jasm.parse_operand @dest, dest

	if @dest.type = 'reg'
		; reg, imm
		if @src.type = 'imm'
			dw 2ah
			dw (dest metadata 1 - jasm.reg)
		end if
	else 
		err 'destination must be a register'
	end if 
end macro 

; sll 
macro sll? dest*, src* 
	jasm.parse_operand @dest, dest
	jasm.parse_operand @src, src
	
	if @dest.type = 'reg'
		; reg, imm
		if @src.type = 'imm'
			dw 2bh, (dest metadata 1 - jasm.reg)
			dw @src.imm
		; reg, reg
		else if @src.type = 'reg'
			dw 2ch, (dest metadata 1 - jasm.reg)
			dw (src metadata 1 - jasm.reg)
		end if
	else 
		err 'destination must be a register'
	end if 
end macro 

; srl 
macro srl? dest*, src* 
	jasm.parse_operand @dest, dest
	jasm.parse_operand @src, src
	
	if @dest.type = 'reg'
		; reg, imm
		if @src.type = 'imm'
			dw 2dh, (dest metadata 1 - jasm.reg)
			dw @src.imm
		; reg, reg
		else if @src.type = 'reg'
			dw 2eh, (dest metadata 1 - jasm.reg)
			dw (src metadata 1 - jasm.reg)
		end if
	else 
		err 'destination must be a register'
	end if 
end macro 

; sla 
macro sla? dest*, src* 
	jasm.parse_operand @dest, dest
	jasm.parse_operand @src, src
	
	if @dest.type = 'reg'
		; reg, imm
		if @src.type = 'imm'
			dw 2fh, (dest metadata 1 - jasm.reg)
			dw @src.imm
		; reg, reg
		else if @src.type = 'reg'
			dw 30h, (dest metadata 1 - jasm.reg)
			dw (src metadata 1 - jasm.reg)
		end if
	else 
		err 'destination must be a register'
	end if 
end macro 

; sra 
macro sra? dest*, src* 
	jasm.parse_operand @dest, dest
	jasm.parse_operand @src, src
	
	if @dest.type = 'reg'
		; reg, imm
		if @src.type = 'imm'
			dw 31h, (dest metadata 1 - jasm.reg)
			dw @src.imm
		; reg, reg
		else if @src.type = 'reg'
			dw 32h, (dest metadata 1 - jasm.reg)
			dw (src metadata 1 - jasm.reg)
		end if
	else 
		err 'destination must be a register'
	end if 
end macro 

; rol 
macro rol? dest*, src* 
	jasm.parse_operand @dest, dest
	jasm.parse_operand @src, src
	
	if @dest.type = 'reg'
		; reg, imm
		if @src.type = 'imm'
			dw 33h 
			dw (dest metadata 1 - jasm.reg)
			dw @src.imm
		; reg, reg
		else if @src.type = 'reg'
			dw 34h, (dest metadata 1 - jasm.reg)
			dw (src metadata 1 - jasm.reg)
		end if
	else 
		err 'destination must be a register'
	end if 
end macro 

; ror 
macro ror? dest*, src* 
	jasm.parse_operand @dest, dest
	jasm.parse_operand @src, src
	
	if @dest.type = 'reg'
		; reg, imm
		if @src.type = 'imm'
			dw 35h, (dest metadata 1 - jasm.reg)
			dw @src.imm
		; reg, reg
		else if @src.type = 'reg'
			dw 36h, (dest metadata 1 - jasm.reg)
			dw (src metadata 1 - jasm.reg)
		end if
	else 
		err 'destination must be a register'
	end if 
end macro 

; jne - if cmp is not equal
macro jne? dest*
	jasm.parse_operand @dest, dest

	; imm 
	if @dest.type = 'imm'
		dw 37h
		dw @dest.imm
	; reg
	else if @dest.type = 'reg'	
		dw 38h
		dw (dest metadata 1 - jasm.reg)
	else 
		err 'invalid destination'
	end if
end macro 

; jg - if cmp is greater
macro jg? dest*
	jasm.parse_operand @dest, dest

	; imm 
	if @dest.type = 'imm'
		dw 39h
		dw @dest.imm
	; reg
	else if @dest.type = 'reg'	
		dw 3ah
		dw (dest metadata 1 - jasm.reg)
	else 
		err 'invalid destination'
	end if
end macro 

; jl - if cmp is less
macro jl? dest*
	jasm.parse_operand @dest, dest

	; imm 
	if @dest.type = 'imm'
		dw 3bh
		dw @dest.imm
	; reg
	else if @dest.type = 'reg'	
		dw 3ch
		dw (dest metadata 1 - jasm.reg)
	else 
		err 'invalid destination'
	end if
end macro 

; data bus communication 
; register r8 is used for bus address

; in dest, src
macro in? dest*, src*
	jasm.parse_operand @dest, dest
	jasm.parse_operand @src, src

	; reg
	if @src.type = 'reg'	
		dw 3dh
		dw @dest.imm ; port
		dw (src metadata 1 - jasm.reg) ; data
	else 
		err 'src has to be a register'
	end if
end macro 

; out dest, src
macro out? dest*, src*
	jasm.parse_operand @dest, dest
	jasm.parse_operand @src, src

	; imm 
	if @src.type = 'imm'
		dw 3eh
		dw @dest.imm ; port
		dw @src.imm ; data
	; reg
	else if @src.type = 'reg'	
		dw 3fh
		dw @dest.imm ; port
		dw (src metadata 1 - jasm.reg) ; data
	else 
		err 'invalid destination'
	end if
end macro 


































