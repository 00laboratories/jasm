include 'jasm.inc'

; mapped devices 
; port 0 - GPU 
; port 1 - RAM
; port 2 - ROM 
; port 3 - 7 segment

start:
    ; delay count by FFFF
    add r2, 1
    cmp r2, 0xFFFF
    jl start 
    ; reset counter, unnecessary
    xor r2, r2
    ; increment value to led's by 1
    add r1,1
    out 3, r1 ; write to leds
jmp start