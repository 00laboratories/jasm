include 'jasm.inc'

; mapped devices 
; port 0 - GPU 
; port 1 - RAM
; port 2 - ROM 
; port 3 - LED

start:
    ; reg imm 
    mov r1, 0xffff
    mov r3, 0x3333
    ; reg reg 
    mov r2, r1 
    ; [imm] imm 
    mov [variable1], 0xaabb
    mov r1, [variable1]
    ; [reg], imm
    mov r1, variable1
    mov [r1], 0x1111
    mov r1, [variable1]
    ; [imm], reg 
    mov [variable1], r3
    mov r4, [variable1]
    ; [reg], reg
    mov r1, variable1
    mov [r1], r2 ; <----- r2
    mov r1, [variable1]
    ; reg, [imm]
    mov r3, [variable1]
    ; reg, [reg]
    mov r1, variable1
    mov r4, [r1]
    ; is the last mov to r4 the same vlaue as r2
    cmp r4, r2 
    je mov_success
    out 3, 0xdead

variable1: dw 0

tests_passed:
    mov r1, 0xA
    cmp r1, 0xF
    jg jg_works
    out 3, 0xeee1 ; error
jg_works:
    cmp r1, 0x01
    jl jl_works
    out 3, 0xeee2 ; error
jl_works:
jmp jl_works

ret   

mov_success:
    push 1337
    pop r2 
    cmp r2, 1337
    je push_pop_success
    out 3, 0xddee
    push_pop_success: 
    ; test math functions 
    mov r1, 0
    mul r2,r1
    add r1, 2 ; 0x2
    sub r1, 1 ; 0x1
    mul r1, 10 ; 0xA
    div r1, 2 ; 0x5
    and r1, 0x1 ; 0x1
    or r1, 14 ; 0xF 
    nand r1, 3; 0xFFFC
    nor r1, 5; 0x0002
    xor r1, 0xfff0 ; 0xfff2
    sll r1, 8 ; 0xF200
    srl r1, 3 ; 0x1E40
    rol r1, 8 ; 0x401E
    ror r1, 4 ; 0xE401

    call tests_passed

    ; IN and OUT 
    mov r8, t_var1 ; addr of t_var1
    out 1, 0xff ; write 0xff to bus port 1
    in 1, r5 ; read bus port 1 into r5
    cmp r5, 0xff
    je bus_works
    out 3, 0xcece
    ; all tests success 
bus_works:
    out 3, 0xb00b 
    jmp bus_works

t_var1: dw 0x0
    