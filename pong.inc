
; prevent cpu from starting in the file 
jmp end_of_pong_inc

; player global settings 
define player_graphics_half_height 24 
define player_graphics_width 4 
define player_graphics_color 0xff

; specific player variables
player1_pos_y: dw (240/2)
player2_pos_y: dw (240/2)

player_1_delay: dw 0
player_2_delay: dw 0

; ball 
define ball_size 4
define ball_color 0xff

ball_x: dw (320/2)
ball_y: dw (240/2)
ball_vel_x: dw 0
ball_vel_y: dw 0
ball_delay: dw 0

; r1 to r5 is used and modified 
draw_player_1:
    mov r1, 10 ; player 1 x pos 
    mov r3, player_graphics_color ; color 
    xor r5, r5 ; keep track of x drawing
draw_player_1_newline:
    mov r2, [player1_pos_y] ; y pos
    sub r2, player_graphics_half_height ; start drawing from top 
    xor r4, r4 ; keep track of y drawing
draw_player_1_draw_y:
    cmp r4, (player_graphics_half_height*2)
    add r4, 1
    add r2, 1 
    drawPixel r1, r2, r3
    jne draw_player_1_draw_y
draw_player_1_increment_x:
    add r5, 1
    add r1, 1
    cmp r5, player_graphics_width
    jne draw_player_1_newline
ret ; return from function 

; r1 to r5 is used and modified 
clear_player_1:
    mov r1, 10 ; player 1 x pos 
    mov r3, 0 ; color 
    xor r5, r5 ; keep track of x drawing

clear_player_1_newline:
    mov r2, [player1_pos_y] ; y pos
    sub r2, player_graphics_half_height ; start drawing from top 
    xor r4, r4 ; keep track of y drawing
clear_player_1_clear_y:
    cmp r4, (player_graphics_half_height*2)
    add r4, 1
    add r2, 1 
    drawPixel r1, r2, r3
    jne clear_player_1_clear_y
clear_player_1_increment_x:
    add r5, 1
    add r1, 1
    cmp r5, player_graphics_width
    jne clear_player_1_newline
ret ; return from function 

; r1 to r5 is used and modified 
clear_player_2:
    mov r1, (320-10-player_graphics_width)
    mov r3, 0 ; color 
    xor r5, r5 ; keep track of x drawing

clear_player_2_newline:
    mov r2, [player2_pos_y] ; y pos
    sub r2, player_graphics_half_height ; start drawing from top 
    xor r4, r4 ; keep track of y drawing
clear_player_2_clear_y:
    cmp r4, (player_graphics_half_height*2)
    add r4, 1
    add r2, 1 
    drawPixel r1, r2, r3
    jne clear_player_2_clear_y
clear_player_2_increment_x:
    add r5, 1
    add r1, 1
    cmp r5, player_graphics_width
    jne clear_player_2_newline
ret ; return from function 

update_player_1:
    mov r1, [player_1_delay]
    cmp r1, 128
    jne update_player_1_nope
    mov [player_1_delay], 0

    call clear_player_1

    ;read key input 
    in 4, r6  
    nand r6, 0x3 ; read only first two keys 
    and r6, 0x3 ; keys are inverted 
    cmp r6, 2 
    je update_player_1_move_up
    cmp r6, 1
    je update_player_1_move_down
    ; else
    jmp update_player_1_end
update_player_1_move_up:  
    mov r2, [player1_pos_y]

    ; do not move outside screen 
    cmp r2, player_graphics_half_height 
    jg update_player_1_end

    sub r2, 1
    mov [player1_pos_y], r2 
    jmp update_player_1_end
update_player_1_move_down:
    mov r2, [player1_pos_y]

    ; do not move outside screen 
    cmp r2, 237-player_graphics_half_height 
    jl update_player_1_end

    add r2, 1
    mov [player1_pos_y], r2 
update_player_1_end:
    call draw_player_1
    ret
update_player_1_nope:
    add r1, 1
    mov [player_1_delay], r1
ret

update_player_2:
    mov r1, [player_2_delay]
    cmp r1, 128
    jne update_player_2_nope
    mov [player_2_delay], 0

    call clear_player_2

    ;read key input 
    in 4, r6  
    nand r6, 0xC ; read only second two keys 
    and r6, 0xC ; keys are inverted 
    cmp r6, 8 
    je update_player_2_move_up
    cmp r6, 4
    je update_player_2_move_down
    ; else
    jmp update_player_2_end
update_player_2_move_up:  
    mov r2, [player2_pos_y]

    ; do not move outside screen 
    cmp r2, player_graphics_half_height 
    jg update_player_2_end

    sub r2, 1
    mov [player2_pos_y], r2 
    jmp update_player_2_end
update_player_2_move_down:
    mov r2, [player2_pos_y]

    ; do not move outside screen 
    cmp r2, 237-player_graphics_half_height 
    jl update_player_2_end

    add r2, 1
    mov [player2_pos_y], r2 
update_player_2_end:
    call draw_player_2
    ret
update_player_2_nope:
    add r1, 1
    mov [player_2_delay], r1
ret

; r1 to r5 is used and modified 
draw_player_2:
    mov r1, (320-10-player_graphics_width) ; player 1 x pos 
    mov r3, player_graphics_color ; color 
    xor r5, r5 ; keep track of x drawing  
draw_player_2_newline:
    mov r2, [player2_pos_y] ; y pos
    sub r2, player_graphics_half_height ; start drawing from top 
    xor r4, r4 ; keep track of y drawing
draw_player_2_draw_y:
    cmp r4, (player_graphics_half_height*2)
    add r4, 1
    add r2, 1 
    drawPixel r1, r2, r3
    jne draw_player_2_draw_y
draw_player_2_increment_x:
    add r5, 1
    add r1, 1
    cmp r5, player_graphics_width
    jne draw_player_2_newline
ret ; return from function 

; r1 to r5 is used
draw_ball:
    mov r1, [ball_x]
    sub r1, (ball_size/2)
    mov r3, ball_color
    xor r5, r5 ; keep track of x drawing  
draw_ball_newline:
    mov r2, [ball_y]
    sub r2, (ball_size/2)
    xor r4, r4 ; keep track of y drawing
draw_ball_draw_y:
    cmp r4, ball_size
    add r4, 1
    add r2, 1 
    drawPixel r1, r2, r3
    jne draw_ball_draw_y
draw_ball_increment_x:
    add r5, 1
    add r1, 1
    cmp r5, ball_size
    jne draw_ball_newline
ret 

; r1 to r5 is used
clear_ball:
    mov r1, [ball_x]
    sub r1, (ball_size/2)
    mov r3, 0
    xor r5, r5 ; keep track of x drawing  
clear_ball_newline:
    mov r2, [ball_y]
    sub r2, (ball_size/2)
    xor r4, r4 ; keep track of y drawing
clear_ball_draw_y:
    cmp r4, ball_size
    add r4, 1
    add r2, 1 
    drawPixel r1, r2, r3
    jne clear_ball_draw_y
clear_ball_increment_x:
    add r5, 1
    add r1, 1
    cmp r5, ball_size
    jne clear_ball_newline
ret 

update_ball:
    mov r1, [ball_delay]
    cmp r1, 128
    jne no_move_ball
    mov [ball_delay], 0

    ; clear ball before moving it 
    call clear_ball

    mov r1, [ball_x]
    mov r2, [ball_vel_x]
    cmp r2, 1 
    je move_ball_right ; if vel is 1 go to move ball right otherwise move left
move_ball_left:
    sub r1, 1
    mov [ball_x], r1
    ;collision with left player
    mov r2, [player1_pos_y]
    sub r2, player_graphics_half_height
    mov r3, [ball_y]
    cmp r2, r3
    jl ball_no_collision_with_player_1
      
    mov r2, [player1_pos_y]
    add r2, player_graphics_half_height*2
    
    cmp r2, r3 
    jg ball_no_collision_with_player_1
        
; if collision with player 1 
ball_collision_with_player_1:
    cmp r1, (10+player_graphics_width)
    jl move_ball_y
    mov [ball_vel_x], 1
    jmp move_ball_y
ball_no_collision_with_player_1:
    cmp r1, 0
    jne move_ball_y
    mov [ball_x], (320/2)
    mov [ball_y], (240/2)
    mov [ball_vel_x], 1
    jmp move_ball_y

move_ball_right:
    add r1, 1
    mov [ball_x], r1

    ;collision with left player
    mov r2, [player2_pos_y]
    sub r2, player_graphics_half_height
    mov r3, [ball_y]
    cmp r2, r3
    jl ball_no_collision_with_player_2
      
    mov r2, [player2_pos_y]
    add r2, player_graphics_half_height*2
    
    cmp r2, r3 
    jg ball_no_collision_with_player_2
    
; if collision with player 2 
ball_collision_with_player_2:
    cmp r1, 320 - player_graphics_width - 10
    jg move_ball_y
    mov [ball_vel_x], 0
    jmp move_ball_y
ball_no_collision_with_player_2:
    cmp r1, 320-ball_size
    jne move_ball_y
    mov [ball_x], (320/2)
    mov [ball_y], (240/2)
    mov [ball_vel_x], 0
    jmp move_ball_y

move_ball_y:
    mov r1, [ball_y]
    mov r2, [ball_vel_y]
    cmp r2, 1 
    je move_ball_down ; if vel is 1 go to move ball right otherwise move left
move_ball_up:  
    sub r1, 1
    mov [ball_y], r1
    ; collision with up 
    cmp r1, (ball_size)
    jne updat_ball_end
    mov [ball_vel_y], 1
    jmp updat_ball_end 
move_ball_down:
    add r1, 1
    mov [ball_y], r1
    ; collision with right 
    cmp r1, (240-ball_size)
    jne updat_ball_end
    mov [ball_vel_y], 0
    jmp updat_ball_end
no_move_ball:
    add r1, 1 
    mov [ball_delay], r1
updat_ball_end:
    call draw_ball
ret

end_of_pong_inc: