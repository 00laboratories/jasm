include 'jasm.inc'
include 'pong.inc'

; mapped devices 
; port 0 - GPU 
; port 1 - RAM
; port 2 - ROM 
; port 3 - 7 segment
; port 4 - KEY input

out 3, 0xd00d

jmp game_loop

clear_screen:
    mov r1,0
    mov r2,0
    mov r3,0
clear_loop:
    drawPixel r1, r2, r3
    add r1, 1 ; increment x
    cmp r1, 320 
    jne clear_loop
    add r2, 1 ; increment y
    cmp r2, 240 ; clear until reaches end 
    jne clear_loop



game_loop:
    ; functions defined in pong.inc
    
    ; players
    call update_player_1 
    call update_player_2 
    ; ball
    call update_ball

jmp game_loop
